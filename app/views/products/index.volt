
{{ content() }}

<div align="right" style="margin-top: 20px;">
    {{ link_to("products/new", "Create Products", "class": "btn btn-primary") }}
</div>

{{ form("products/search") }}

<h2>Search products</h2>

<fieldset>

{% for element in form %}
    {% if is_a(element, 'Phalcon\Forms\Element\Hidden') %}
{{ element }}
    {% else %}
<div class="form-group row">
    {{ element.label(['class': 'col-sm-2 col-form-label']) }}
    <div class="col-sm-10">
        {{ element.render(['class': 'form-control']) }}
    </div>
</div>
    {% endif %}
{% endfor %}

<div class="control-group" align="right">
    {{ submit_button("Search", "class": "btn btn-secondary") }}
</div>

</fieldset>

</form>
