
{{ content() }}

{{ form("companies/create") }}

    <ul class="pager">
        <li class="previous pull-left">
            {{ link_to("companies", "&larr; Go Back") }}
        </li>
        <li class="pull-right">
            {{ submit_button("Save", "class": "btn btn-success") }}
        </li>
    </ul>

    <fieldset>

    {% for element in form %}
        {% if is_a(element, 'Phalcon\Forms\Element\Hidden') %}
            {{ element }}
        {% else %}
            <div class="form-group row">
                {{ element.label(['class': 'col-sm-2 col-form-label']) }}
                <div class="col-sm-10">
                    {{ element.render(['class': 'form-control']) }}
                </div>
            </div>
        {% endif %}
    {% endfor %}

    </fieldset>

</form>
