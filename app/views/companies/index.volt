
{{ content() }}

<div align="right" style="margin-top: 20px;">
    {{ link_to("companies/new", "Create Companies", "class": "btn btn-primary") }}
</div>

{{ form("companies/search") }}

<h2>Search companies</h2>

<fieldset>

{% for element in form %}
    {% if is_a(element, 'Phalcon\Forms\Element\Hidden') %}
{{ element }}
    {% else %}
<div class="form-group row">
    {{ element.label(['class': 'col-sm-2 col-form-label']) }}
    <div class="col-sm-10">
        {{ element.render(['class': 'form-control']) }}
    </div>
</div>
    {% endif %}
{% endfor %}

<div class="control-group" align="right">
    {{ submit_button("Search", "class": "btn btn-secondary") }}
</div>

</fieldset>

</form>
