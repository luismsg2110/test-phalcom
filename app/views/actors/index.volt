{{ content() }}

<div align="right" style="margin-top: 20px;">
    {{ link_to("actors/new", "Create actor", "class": "btn btn-primary") }}
</div>

{{ form("actors/search", "autocomplete": "off") }}

<div class="center scaffold">

    <h2>Search actors</h2>

    <div class="form-group row">
        <label for="id" class="col-sm-2 col-form-label">Id</label>
        <div class="col-sm-10">
            {{ form.render('id', ['class': 'form-control']) }}
        </div>
    </div>

    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
            {{ form.render('name', ['class': 'form-control']) }}
        </div>
    </div>

    <div class="control-group" align="right">
        {{ submit_button("Search", "class": "btn btn-secondary") }}
    </div>

</div>

</form>
