{{ content() }}

<div align="right" style="margin-top: 20px;">
    {{ link_to("movies/new", "Create Movies", "class": "btn btn-primary") }}
</div>

{{ form("movies/search") }}

<h2>Search movies</h2>

<fieldset>

<div class="form-group row">
    <label for="id" class="col-sm-2 col-form-label">Id</label>
    <div class="col-sm-10">
        {{ form.render('id', ['class': 'form-control']) }}
    </div>
</div>

<div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Name</label>
    <div class="col-sm-10">
        {{ form.render('name', ['class': 'form-control']) }}
    </div>
</div>

<div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Director</label>
    <div class="col-sm-10">
        {{ form.render('director_id', ['class': 'form-control']) }}
    </div>
</div>

<div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Selecciones un actor</label>
    <div class="col-sm-10">
        {{ form.render('actors_ids[]', ['class': 'form-control selectpicker']) }}
    </div>
</div>


<div class="control-group" align="right">
    {{ submit_button("Search", "class": "btn btn-secondary") }}
</div>

</fieldset>

</form>
