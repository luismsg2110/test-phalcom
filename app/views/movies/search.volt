{{ content() }}

<ul class="pager">
    <li class="previous">
        {{ link_to("movies", "&larr; Go Back") }}
    </li>
    <li class="next">
        {{ link_to("movies/new", "Create movies") }}
    </li>
</ul>

{% for movie in page.items %}
    {% if loop.first %}
<table class="table table-bordered table-striped" align="center">
    <thead>
        <tr>
            <th>Id</th>
            <th>Name</th>
            <th>Director</th>
            <th>Actor(es)</th>
        </tr>
    </thead>
    <tbody>
    {% endif %}
        <tr>
            <td>{{ movie.id }}</td>
            <td>{{ movie.name }}</td>
            <td>{{ movie.getDirectors().name }}</td>

            <td>{% for actor in movie.getActors() %}
                {{ actor }}
                {% endfor %}
            </td>

            <td width="7%">{{ link_to("movies/edit/" ~ movie.id, '<i class="glyphicon glyphicon-edit"></i> Edit', "class": "btn btn-default") }}</td>
            <td width="7%">{{ link_to("movies/delete/" ~ movie.id, '<i class="glyphicon glyphicon-remove"></i> Delete', "class": "btn btn-default") }}</td>
        </tr>
    {% if loop.last %}
    </tbody>
    <tbody>
        <tr>
            <td colspan="7" align="right">
                <div class="btn-group">
                    {{ link_to("movies/search", '<i class="icon-fast-backward"></i> First', "class": "btn") }}
                    {{ link_to("movies/search?page=" ~ page.before, '<i class="icon-step-backward"></i> Previous', "class": "btn") }}
                    {{ link_to("movies/search?page=" ~ page.next, '<i class="icon-step-forward"></i> Next', "class": "btn") }}
                    {{ link_to("movies/search?page=" ~ page.last, '<i class="icon-fast-forward"></i> Last', "class": "btn") }}
                    <span class="help-inline">{{ page.current }} of {{ page.total_pages }}</span>
                </div>
            </td>
        </tr>
    </tbody>
</table>
    {% endif %}
{% else %}
    No movies are recorded
{% endfor %}
