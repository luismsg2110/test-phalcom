<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\Numericality;

class MoviesForm extends Form
{
    /**
     * Initialize the products form
     */
    public function initialize($entity = null, $options = array())
    {
        if (!isset($options['edit'])) {
            $element = new Text("id");
            $this->add($element->setLabel("Id"));
        } else {
            $this->add(new Hidden("id"));
        }

        $name = new Text("name");
        $name->setLabel("Name");
        $name->setFilters(['striptags', 'string']);
        $name->addValidators([
            new PresenceOf([
                'message' => 'Name is required'
            ])
        ]);
        $this->add($name);

        $director = new Select('director_id', Directors::find(), [
            'using'      => ['id', 'name'],
            'useEmpty'   => true,
            'emptyText'  => 'Seleccione un director',
            'emptyValue' => ''
        ]);
        $director->setLabel('Director');
        $this->add($director);

        $actors = new Select('actors_ids[]', Actors::find(), [
            'using'      => ['id', 'name'],
            'useEmpty'   => true,
            'multiple'   => true,
            'emptyText'  => 'Seleccione actores',
            'emptyValue' => ''
        ]);

        if (isset($entity->actors_ids)) {
            $this->selected = explode(",", $entity->actors_ids);
            $actors->setDefault($this->selected);
        } else {
            $this->selected = '1';
        }    
        
        $actors->setLabel('Actores');
        $this->add($actors);
    }
}
