<?php

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

/**
    7:  * MoviesController
    8   *
    9   * Manage operations for movies
   10   */

class MoviesController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Manage your movies');
        parent::initialize();
    }

    /**
     * Shows the index action
     */
    public function indexAction()
    {
        $this->session->conditions = null;
        $this->view->form = new MoviesForm;
    }

    /**
     * Search companies based on current criteria
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            if (isset($this->request->getPost()['actors_ids'])) {
                $this->requesform = $this->request->getPost();
                $this->rq = $this->request->getPost()['actors_ids'][0];
                $this->requesform['actors_ids'] = $this->rq; 
            } else {
               $this->requesform = $this->request->getPost();
            }

            $query = Criteria::fromInput($this->di, "Movies", $this->requesform);
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = array();
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $movies = Movies::find($parameters);
        if (count($movies) == 0) {
            $this->flash->notice("The search did not find any movie");

            return $this->dispatcher->forward(
                [
                    "controller" => "movies",
                    "action"     => "index",
                ]
            );
        }

        $paginator = new Paginator(array(
            "data"  => $movies,
            "limit" => 10,
            "page"  => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Shows the form to create a new actor
     */
    public function newAction()
    {
        $this->view->form = new MoviesForm(null, ['edit' => true]);
    }

    /**
     * Edits a actor based on its id
     */
    public function editAction($id)
    {
         if (!$this->request->isPost()) {

            $movie = Movies::findFirstById($id);
            if (!$movie) {
                $this->flash->error("Movie was not found");

                return $this->dispatcher->forward(
                    [
                        "controller" => "movies",
                        "action"     => "index",
                    ]
                );
            }

            $this->view->form = new MoviesForm($movie, array('edit' => true));
        }
    }

    /**
     * Creates a new actor
     */
    public function createAction()
    {
        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                [
                    "controller" => "movies",
                    "action"     => "index",
                ]
            );
        }

        $form = new MoviesForm;
        $movie = new Movies();

        $data = $this->request->getPost();

        if (!$form->isValid($data, $movie)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "movies",
                    "action"     => "new",
                ]
            );
        } 

        
        if (isset($data['actors_ids'])) {
            $actores = $data['actors_ids'];
            $actores = join(",",$actores);
            $movie->actors_ids = $actores;
        } else {
            $movie->actors_ids = '1';
        }

        if ($movie->save() == false) {
            foreach ($movie->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "movies",
                    "action"     => "new",
                ]
            );
        }


        $form->clear();

        $this->flash->success("Movie was created successfully");

        return $this->dispatcher->forward(
            [
                "controller" => "movies",
                "action"     => "index",
            ]
        );
    }

    /**
     * Saves current actor in screen
     *
     * @param string $id
     */
    public function saveAction()
    {
       if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                [
                    "controller" => "movies",
                    "action"     => "index",
                ]
            );
        }

        $id = $this->request->getPost("id", "int");

        $movie = Movies::findFirstById($id);
        if (!$movie) {
            $this->flash->error("Movie does not exist");

            return $this->dispatcher->forward(
                [
                    "controller" => "movies",
                    "action"     => "index",
                ]
            );
        }

        $form = new MoviesForm;
        $this->view->form = $form;

        $data = $this->request->getPost();

        if (!$form->isValid($data, $movie)) {
            foreach ($form->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "movies",
                    "action"     => "edit",
                    "params"     => [$id]
                ]
            );
        }

        if (isset($data['actors_ids'])) {
            $actores = $data['actors_ids'];
            $actores = join(",",$actores);
            $movie->actors_ids = $actores;
        }
        

        if ($movie->save() == false) {
            foreach ($movie->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "movies",
                    "action"     => "edit",
                    "params"     => [$id]
                ]
            );
        }

        $form->clear();

        $this->flash->success("Movie was updated successfully");

        return $this->dispatcher->forward(
            [
                "controller" => "movies",
                "action"     => "index",
            ]
        ); 
    }

    /**
     * Deletes a actor
     *
     * @param string $id
     */
    public function deleteAction($id)
    {
        $movie = Movies::findFirstById($id);
        if (!$movie) {
            $this->flash->error("Movie was not found");

            return $this->dispatcher->forward(
                [
                    "controller" => "movies",
                    "action"     => "index",
                ]
            );
        }

        if (!$movie->delete()) {
            foreach ($movie->getMessages() as $message) {
                $this->flash->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "movies",
                    "action"     => "search",
                ]
            );
        }

        $this->flash->success("Movie was deleted");

            return $this->dispatcher->forward(
                [
                    "controller" => "movies",
                    "action"     => "index",
                ]
            );
    }
}
