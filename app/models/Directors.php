<?php

use Phalcon\Mvc\Model;

/**
 * Types of Directors
 */
class Directors extends Model
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    public function initialize()
    {
        $this->hasOne('id', 'Movies', 'director_id', [
        	'foreignKey' => [
        		'message' => 'Director cannot be deleted because it\'s used in Movies'
        	]
        ]);
    }

}
