<?php

use Phalcon\Mvc\Model;

/**
 * Products
 */
class Movies extends Model
{
	/**
	 * @var integer
	 */
	public $id;

	/**
	 * @var integer
	 */
	public $director_id;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $actors_ids;

	/**
	 * Movies initializer
	 */
	public function initialize()
	{
		$this->belongsTo('director_id', 'Directors', 'id', [
			'reusable' => true
		]);

		$this->belongsTo('actors_ids', 'Actors', 'id', [
			'reusable' => true
		]);
	}

	
	public function getActors()
	{
		$this->actors_ids = explode(",", $this->actors_ids);
		$miarray = array();
		 	foreach ($this->actors_ids as $value) { 
		 		$conditions = ['id'=>$value];
		 		$actor = Actors::findFirstById($value);
		 		array_push($miarray, $actor->name);
		 	}
		return $miarray;
	}
}
