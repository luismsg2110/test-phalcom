<?php

use Phalcon\Mvc\Model;

/**
 * Types of Actors
 */
class Actors extends Model
{
    /**
     * @var integer
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * Actors initializer
     */
    public function initialize()
    {
        $this->hasMany('id', 'Movies', 'actors_ids', [
        	'foreignKey' => [
        		'message' => 'Actor cannot be deleted because it\'s used in Movies'
        	]
        ]);
    }

}
